/**
 * @brief 测试c++中的切割问题
 */
#include <iostream>

using namespace std;

class Test {
public:
    virtual void func() const { cout << "Test" << endl; }
};

class TestSub : public Test {
public:
    void func() const override { cout << "TestSub" << endl; }
};

void test(Test t) { t.func(); }
void test_sub(TestSub t) { t.func(); }
void test_ref(const Test& t) { t.func(); }

int main() {
    Test t1;
    TestSub t2;

    test(t1);  // output: Test
    test(t2);  // output: Test  // 派生类被切割为只保留基类的部分

    test_ref(t1);  // output: Test
    test_ref(t2);  // output: TestSub

    // test_sub(t1);  // 基类无法转化为派生类
    test_sub(t2);
}