/**
 * @brief 测试queue
*/


#include <queue>
#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"
#define FMT_HEADER_ONLY
#include <fmt/format.h>


TEST_CASE("Queue: pop order")
{
    using namespace std;
    queue<int> q;

    for (int i = 0; i < 10; i += 2)
        q.push(i);

    while (!q.empty())
    {
        fmt::print("{} ", q.front());
        q.pop();
        // output: 0 2 4 6 8
    }
}