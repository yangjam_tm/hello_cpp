#include <iostream>

using namespace std;

class Base {

public:
    virtual void func() { cout << "func" << endl;}
    void func(int val) { cout << "get arg: " << val << endl;}
};

class Derived : public Base{
public:
    void func() override { cout << "derived" << endl;}
    void func(int val) {
        Base::func(val);
    }
};

int main()
{
    Base b;
    Derived d;

    d.func();
    d.func(5);  // 该函数被派生类同名函数遮掩

    return 0;
}