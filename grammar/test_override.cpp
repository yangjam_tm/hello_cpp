#include <iostream>

using namespace std;

class Base {
public:
    void func() { cout << "Base" << endl; }
};

class Derived : public Base {
public:
    void func() { cout << "Derived" << endl; }
};

int main() {
    Base a;
    Derived b;
    Base* pb = &b;

    a.func();
    b.func();
    pb->func();
}