/**
 * @brief 比较数组
 */
#include <iostream>

using namespace std;

int main(void) {
    int arr1[5] = {1, 2, 3, 4, 5};
    int arr2[5] = {1, 2, 3, 4, 5};

    char str1[]{"--version"};
    char str2[] = "--version";

    // !c++不支持数组直接比较，触发 warnning`comparison between two array`
    // !两者之间的比较实际上是比较基地址:
    // `note: use unary ‘+’ which decays operands to pointers or ‘&arr1[0] ==
    // &arr2[0]’ to compare the addresses`
    cout << boolalpha << (arr1 == arr2) << endl;  // result: false
    // 字符串常量，相同字符串保存在同一地址
    cout << boolalpha << ("--version" == "--version") << endl;  // result: true
    // str1和str2声明为变量，分配在栈空间
    cout << boolalpha << (str1 == str2) << endl;         // result: false
    cout << boolalpha << (str2 == "--version") << endl;  // result: false

    return 0;
}