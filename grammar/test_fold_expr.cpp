/**
 * @brief 折叠表达式
 */

#include <initializer_list>
#include <iostream>
#include <string>
#include <type_traits>

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

#if __cplusplus >= 201703L
// `typenameT&... T` 表示0和或多个类型为T的引用，该模板支持不定参数
template <typename First, typename... T>
inline bool is_in(const First& first, const T&... t) {
    // `...`
    // c++17引入的折叠表达式，将对参数列表中不定参数列表中的元素进行遍历，重复之前的操作
    // 若t被初始化为{t1, t2, t3}，则该表达式在编译时将翻译为 first == t1 or
    // first == t2 or first == t3
    return ((first == t) or ...);
}

#elif __cplusplus >= 201103L

// 当参数T不存在时，实例化该模板
template <size_t Index = 0, typename First, typename T>
typename std::enable_if<Index == std::tuple_size<T>::value, bool>::type
is_in_impl(const First& first, const T& t) {
    return false;
}

// 当不定参数列表T存在时，实例化该模板；并在Index遍历参数列表过程中实例化新的模板函数
template <size_t Index = 0, typename First, typename T>
    typename std::enable_if <
    Index<std::tuple_size<T>::value, bool>::type is_in_impl(const First& first,
                                                            const T& t) {
    return (first == std::get<Index>(t)) or is_in_impl<Index + 1>(first, t);
}

template <typename First, typename... T>
bool is_in(const First& first, const T&... t) {
    return is_in_impl(first, std::make_tuple(t...));
}
// 以is_in(argument, "-v", "--version")为例
// 编译器将首先实例化 Index =0 的两个模板
// 1. enable_if<0 == 2, bool> is_in_impl(...) 0 ==
// 2为假，type不会被定义，该模板被忽略
// 2. enbale_if<0 < 2, bool> is_in_impl(...)  0 <
// 2为真，type定义为bool，该模板被实例化 上述模板实例化过程中，将继续实例化
// Index=1 的模板；同上 直到 Index=2:
// 1. enable_if<2 == 2, bool> is_in_impl(...)
// 被实例化，注意此时参数列表中的t并不存在
// 2. enable_if<2 < 2, bool> is_in_impl(...) 被忽略
#endif

TEST_CASE("fold expression") {
    std::string argument = "-v";

    std::string str_array[] = {"-v", "--version"};
    std::initializer_list<std::string> str_list{"-v", "--version"};

    // First被推断为char[3], T被推断为{char[3]}, 直接比较array会触发`comparison
    // between two array`警告，且数组比较实际上是比较地址
    // CHECK(is_in("-v", "-v"));
    // CHECK(is_in("--version", "--version"));
    // CHECK(is_in("-v", "-v", "--version"));
    CHECK(is_in(argument, "-v", "--version"));
    argument = "--version";
    CHECK(is_in(argument, "-v", "--version"));
    // CHECK(is_in(argument, str_array));  // build failed
    // CHECK(is_in(argument, str_list));   // build failed

    CHECK_FALSE(is_in(argument));
}