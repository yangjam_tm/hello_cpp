#include "smart_ptr.hpp"

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"
#define FMT_HEADER_ONLY
#include <fmt/format.h>

class Test {
public:
    Test() { fmt::print("Test constructor!\n"); }
    virtual ~Test() { fmt::print("Test destory!!!\n"); }

    virtual void func() { fmt::print("Test\n"); }
};

class TestSub : public Test {
public:
    void func() override { fmt::print("I am TestSub!\n"); }
};

TEST_CASE("auto_ptr") {
    using namespace my_std;

    SUBCASE("base function") {
        auto_ptr<Test> pt1(new Test());
        auto_ptr<Test> pt2(pt1);
        auto_ptr<Test> pt3;

        pt3 = pt2;
        pt3->func();
        (*pt3).func();

        CHECK_EQ(pt1.get(), nullptr);
        CHECK_EQ(pt2.get(), nullptr);
    }

    SUBCASE("derived") {
        auto_ptr<Test> pt1(new TestSub());
        auto_ptr<Test> pt2;

        pt2 = pt1;
        pt2->func();
    }
}

TEST_CASE("unique_ptr") {
    using namespace my_std;

    SUBCASE("base func") {
        unique_ptr<Test> pt1(new Test());
        // unique_ptr<Test> pt2(pt1);  // !拷贝构造被禁用
        unique_ptr<Test> pt2(std::move(pt1));
        unique_ptr<Test> pt3;

        // pt3 = pt2; // !拷贝构造被禁用，无法构建形参
        pt3 = std::move(pt2);

        pt3->func();
        (*pt3).func();

        CHECK_EQ(pt1.get(), nullptr);
        CHECK_EQ(pt2.get(), nullptr);
    }

    SUBCASE("derived") {
        unique_ptr<Test> pt1(new TestSub());
        unique_ptr<Test> pt2;

        pt2 = std::move(pt1);
        pt2->func();
    }
}

TEST_CASE("shared ptr") {
    using namespace my_std;

    SUBCASE("base func") {
        shared_ptr<Test> pt1(new Test);
        shared_ptr<Test> pt2;

        pt2 = pt1;

        pt1->func();

        CHECK_EQ(pt2.get_use_count(), 2);
    }
}