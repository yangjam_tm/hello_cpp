/**
 * @brief 测试位运算
 */
#include <iostream>
#include <random>
#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#define FMT_HEADER_ONLY
#include <fmt/format.h>

#include "doctest.h"

TEST_CASE("计算偏移") {
    srand(time(nullptr));
    int a = 1024;
    int b = rand() % 40;
    fmt::print("a = {} b = {}\n", a, b);

    CHECK(0 == (a & (a - 1)));  // 如下等式只在a为2的幂时成立
    CHECK_EQ(b & (a - 1), b % a);
}

TEST_CASE("交换a,b的值") {
    srand(time(nullptr));

    auto swap = [](int& a, int& b) {
        a ^= b;
        b ^= a;
        a ^= b;
    };

    SUBCASE(" ^ 基本逻辑") {
        CHECK_EQ(0 ^ 0, 0);
        CHECK_EQ(0 ^ 1, 1);
        CHECK_EQ(1 ^ 1, 0);

        int val = 123;
        CHECK_EQ((val ^ 0), val);
        CHECK_EQ((val ^ val), 0);
    }

    SUBCASE("a = 0") {
        int a = 0;
        int b = 1024;

        swap(a, b);
        CHECK_EQ(a, 1024);
        CHECK_EQ(b, 0);
    }

    SUBCASE(" a = b") {
        int a = 123;
        int b = 123;

        swap(a, b);

        CHECK_EQ(a, 123);
        CHECK_EQ(b, 123);
    }

    SUBCASE("一般情况") {
        for (int i = 0; i < 100; ++i) {
            int a     = rand() % 1024;
            int b     = rand() % 1024;
            int aTemp = a, bTemp = b;

            swap(a, b);
            CHECK_EQ(a, bTemp);
            CHECK_EQ(b, aTemp);
        }
    }
}