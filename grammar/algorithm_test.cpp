/**
 * @brief <alagorithm>测试
 */

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"
#define FMT_HEADER_ONLY
#include <fmt/format.h>

#include <algorithm>
#include <iterator>
#include <tuple>
#include <vector>

using namespace std;
using namespace fmt;

TEST_CASE("condition test") {
    vector<int> v(24);

    srand(time(nullptr));

    for (auto it = v.begin(); it != v.end(); it++) {
        *it = rand() % 1024;
        print("{} ", *it);
    }
    print("\n");

    CHECK(all_of(v.begin(), v.end(), [](int i) { return i > 0 && i < 1024; }));

    CHECK(any_of(v.begin(), v.end(), [](int i) { return i % 7 == 0; }));

    WARN(none_of(v.begin(), v.end(),
                 [](int i) { return (i % 7 == 0) && (i % 9 == 0); }));
}

TEST_CASE("find test") {
    vector<int> v(24);

    srand(time(nullptr));

    for (auto it = v.begin(); it != v.end(); it++) {
        *it = rand() % 1024;
        print("{} ", *it);
    }
    print("\n");

    auto iter = find_if(v.begin(), v.end(),
                        [](int i) { return i % 7 == 0 && i % 9 == 0; });
    if (iter != v.end()) {
        CHECK((*iter % 7 == 0 && *iter % 9 == 0));
        print("find it: {} \n", *iter);
    }
}

TEST_CASE("find min value") {
    SUBCASE("min") {
        initializer_list<int> indexs{0, 1, 2, 3, 4, 5};

        using TempType = tuple<int, float>;
        vector<TempType> values;

        values.emplace_back(make_tuple(0, 78.5));
        values.emplace_back(make_tuple(1, 88.5));
        values.emplace_back(make_tuple(2, 48.5));
        values.emplace_back(make_tuple(3, 73.5));
        values.emplace_back(make_tuple(4, 82.5));
        values.emplace_back(make_tuple(5, 49.5));

        // min支持初始化列表，但是不支持列表等容器
        auto index = std::min(indexs, [&values](int a, int b) {
            return get<1>(values[a]) < get<1>(values[b]);
        });

        CHECK_EQ(get<0>(values[index]), index);
        CHECK_EQ(get<1>(values[index]), 48.5);
    }

    SUBCASE("min_element") {
        vector<int> indexs{0, 1, 2, 3, 4, 5};

        using TempType = tuple<int, float>;
        vector<TempType> values;

        values.emplace_back(make_tuple(0, 78.5));
        values.emplace_back(make_tuple(1, 88.5));
        values.emplace_back(make_tuple(2, 48.5));
        values.emplace_back(make_tuple(3, 73.5));
        values.emplace_back(make_tuple(4, 82.5));
        values.emplace_back(make_tuple(5, 49.5));

        // min_element支持使用容器
        auto index = *(std::min_element(
            indexs.begin(), indexs.end(), [&values](int a, int b) {
                return get<1>(values[a]) < get<1>(values[b]);
            }));

        CHECK_EQ(get<0>(values[index]), index);
        CHECK_EQ(get<1>(values[index]), 48.5);
    }
}

TEST_CASE("copy_if") {
    SUBCASE("get sub vector") {
        vector<int> origin(12);

        generate(origin.begin(), origin.end(), []() { return rand() % 1024; });

        vector<int> sub;

        auto isOdd = [](int val) { return val % 2 == 0; };

        copy_if(origin.begin(), origin.end(), back_inserter(sub), isOdd);

        for (auto val : sub) print("{}\t", val);
        CHECK(all_of(sub.begin(), sub.end(), isOdd));
    }
}