/**
 * @brief <random>测试
 */
#include <chrono>
#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"
#define FMT_HEADER_ONLY
#include <cstdlib>
#include <ctime>
#include <fmt/format.h>
#include <random>

TEST_CASE("generate random number")
{
    // std::random_device rd;

    SUBCASE("random number in range")
    {
        srand(time(NULL));

        for (size_t i = 0; i < 20; i++)
        {
            int r = rand() % 1024;
            CHECK((r >= 0 && r <= 1024));
            fmt::print("{} ", r);
        }
    }

    SUBCASE("uniform number in range")
    {
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        std::default_random_engine gen(seed);
        // std::normal_distribution<float> dis(0.0, 1.0);
        std::uniform_int_distribution<int> dis(0, 1024);

        for (size_t i = 0; i < 20; i++)
        {
            int r = dis(gen);
            CHECK((r >= 0 && r <= 1024));
            fmt::print("{} ", r);
        }
    }
}