/**
 * @brief 时间处理相关代码片段
 */
#include <ctime>
#include <iostream>

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"
#define FMT_HEADER_ONLY
#include <fmt/format.h>

using namespace std;

TEST_CASE("print time string")
{
    SUBCASE("chrono")
    {
        auto now = chrono::system_clock::now();
        auto in_time_t = chrono::system_clock::to_time_t(now);
        cout << ctime(&in_time_t); // Mon Feb  5 16:23:12 2024
    }

    SUBCASE("tm")
    {
        // time(time_t*): 获取utc时间
        // localtime(time_t*): 将utc时间转化为当前时区时间
        // !localtime存在线程安全问题，可以使用localtime_r替代
        // gmtime: 获取GMT时间，即标准时区时间

        time_t now = time(nullptr);

        tm *localTime = localtime(&now);

        cout << (localTime->tm_year + 1900) << '-'
             << (localTime->tm_mon + 1) << '-'
             << localTime->tm_mday << ' '
             << localTime->tm_hour << ':'
             << localTime->tm_min << ':'
             << localTime->tm_sec << endl; // 2024-2-6 10:16:15
    }

    SUBCASE("strftime")
    {
        // asctime(const tm*): 将tm结构体的时间转化为字符串
        // strftime(...): 将tm结构体时间转化为自定义格式的字符串

        time_t now = time(nullptr);
        cout << asctime(localtime(&now)); // Mon Feb  5 16:23:12 2024

        char timeStr[80]{'\0'};
        strftime(timeStr, 80, "%Y-%m-%d %H:%M:%S", localtime(&now));
        cout << timeStr << endl; // 2024-02-05 16:23:12
    }
}

TEST_CASE("test: clock_gettime")
{
    /**
     * @param tv_sec: 秒
     * @param tv_nsec: 纳秒
     */
    struct timespec ts;

    memset(&ts, 0, sizeof(ts));

    /**
     * @param [in] __clock_id
     * - CLOCK_REALTIME: 获取从1970年1月1日至今的时间
     * - CLOCK_MONOTONIC: 获取系统启动时间
     * - CLOCK_PROCESS_CPUTIME_ID: 获取本进程运行时间
     * - CLOCK_THREAD_CPUTIME_ID: 获取本线程运行时间
     */

    SUBCASE("CLOCK_REALTIME")
    {
        clock_gettime(CLOCK_REALTIME, &ts);
        fmt::print("real time: sec = {} nsec = {}\n", ts.tv_sec, ts.tv_nsec);
        // real time: sec = 1707188334 nsec = 472664453

        fmt::print("{}", ctime(&ts.tv_sec)); // Tue Feb  6 10:58:54 2024
    }

    SUBCASE("CLOCK_MONOTONIC")
    {
        clock_gettime(CLOCK_MONOTONIC, &ts);
        fmt::print("monotonic time: sec = {} nsec = {}\n", ts.tv_sec, ts.tv_nsec);
        // monotonic time: sec = 3357 nsec = 383853385
    }

    SUBCASE("CLOCK_PROCESS_CPUTIME_ID")
    {
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts);
        fmt::print("process cpu time: sec = {} nsec = {}\n", ts.tv_sec, ts.tv_nsec);
        // process cpu time: sec = 0 nsec = 1883024
    }

    SUBCASE("CLOCK_THREAD_CPUTIME_ID")
    {
        clock_gettime(CLOCK_THREAD_CPUTIME_ID, &ts);
        fmt::print("thread cpu time: sec = {} nsec = {}\n", ts.tv_sec, ts.tv_nsec);
        // thread cpu time: sec = 0 nsec = 1896938
    }
}

TEST_CASE("test: measure time")
{
    SUBCASE("chrono")
    {
        auto start = chrono::steady_clock::now();
        sleep(1);
        auto end = chrono::steady_clock::now();

        auto eplased = chrono::duration_cast<chrono::milliseconds>(end - start).count();
        fmt::print("elapsed time: {} ms\n", eplased); // elapsed time: 1000 ms
    }

    SUBCASE("gettimeofday")
    {
        struct timeval start, end;
        gettimeofday(&start, nullptr);
        sleep(1);
        gettimeofday(&end, nullptr);
        auto eplased = (end.tv_sec - start.tv_sec) * 1e3 + (end.tv_usec - start.tv_usec) / 1e3;
        fmt::print("elapsed time: {} ms\n", eplased); // elapsed time: 1000.474 ms
    }

    SUBCASE("clock_gettime: motonic time")
    {
        struct timespec start, end;
        clock_gettime(CLOCK_MONOTONIC, &start);
        sleep(1);
        clock_gettime(CLOCK_MONOTONIC, &end);
        auto eplased = (end.tv_sec - start.tv_sec) * 1e3 + (end.tv_nsec - start.tv_nsec) / 1e6;
        fmt::print("elapsed time: {} ms\n", eplased); // elapsed time: 1000.370835 ms
    }

    SUBCASE("clock_gettime: process time")
    {
        struct timespec start, end;
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);
        sleep(1);
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);
        auto eplased = (end.tv_sec - start.tv_sec) * 1e3 + (end.tv_nsec - start.tv_nsec) / 1e6;
        fmt::print("process cpu time: {} ms\n", eplased); // process cpu time: 0.01844 ms
        // !从结果看只统计cpu运行时间，不包含休眠时间
    }

    SUBCASE("clock_gettime: thread time")
    {
        struct timespec start, end;
        clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start);
        sleep(1);
        clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end);
        auto eplased = (end.tv_sec - start.tv_sec) * 1e3 + (end.tv_nsec - start.tv_nsec) / 1e6;
        fmt::print("thread time: {} ms\n", eplased); // thread time: 0.067646 ms
        // !从结果看只统计cpu运行时间，不包含休眠时间
    }
}