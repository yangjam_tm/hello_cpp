/**
 * @brief 测试unique_lock
 */

#include <atomic>
#include <iostream>
#include <mutex>
#include <thread>

#include <cstdlib>
#include <deque>

using namespace std;

class CTest01
{
public:
    CTest01() = default;
    ~CTest01() = default;

    void Start();
    void End();

private:
    deque<int> m_deq;
    mutex m_mutex;
    thread pushTrd;
    thread popTrd;
    atomic<bool> m_runFlag{true};
};

void CTest01::Start()
{
    srand(time(nullptr));

    pushTrd = thread([this]
                     {
        while (m_runFlag)
        {
            int r = rand() % 2048;
            unique_lock<mutex> lock(m_mutex);
            m_deq.push_back(r);
            lock.unlock();  // 使用unlock解锁，或者等待lock析构时自动释放锁
            this_thread::sleep_for(chrono::milliseconds(r));
        } });

    popTrd = thread([this]
                    {
        while (m_runFlag)
        {
            unique_lock<mutex> lock(m_mutex);
            if (m_deq.empty())
            {
                lock.unlock();
                this_thread::sleep_for(chrono::milliseconds(100));
                continue;
            }
            int r = m_deq.front();
            m_deq.pop_front();
            // lock.release();  //!release只释放mutex的所有权，导致lock析构时无法释放锁，造成死锁
            cout << "pop " << r << endl;
    } });
}

void CTest01::End()
{
    m_runFlag = false;
    pushTrd.join();
    popTrd.join();
}

int main()
{
    CTest01 t;
    t.Start();
    this_thread::sleep_for(chrono::seconds(10));
    t.End();
    return 0;
}