#include <iostream>

using namespace std;

class Test {
public:
    Test(int a, float b) : a_(a), b_(b) { cout << "constructor" << endl; }
    ~Test() = default;

    Test operator=(const Test& rhs) {
        a_ = rhs.a_;
        b_ = rhs.b_;
        return *this;
    }

    friend std::ostream& operator<<(std::ostream& os, const Test& t) {
        os << t.a_ << '\t' << t.b_ << std::endl;
        return os;
    }

private:
    int a_;
    float b_;
};

int main(void) {
    Test t1(12, 1.5);
    Test t2(34, 7.6);
    Test t3(0, 0);

    cout << t1 << t2;

    t3 = t2 = t1;
    cout << t3 << t2;

    return 0;
}