#include <unistd.h>
#include <iostream>
using namespace std;

int main(void)
{
    auto uid = getuid();
    auto euid = geteuid();

    cout << "uid: " << uid << endl;
    cout << "euid: " << euid << endl;
    return 0;
}

// 普通用户权限执行：
/*
uid: 1000
euid: 1000
*/
// root权限执行：
/*
uid: 0
euid: 0
*/