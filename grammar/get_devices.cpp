/**
 * @file get_devices.cpp
 * @author 逆流 (1171267147@qq.com)
 * @brief 读取/dev/下的ttyS设备列表
 * @version 0.1
 * @date 2024-06-12
 *
 * @copyright Copyright (c) 2024
 *
 */

#if __cplusplus >= 201703L
#include <filesystem>
#else
#include <dirent.h>
#endif

#include <iostream>
#include <regex>
#include <vector>

int main()
{
    const char* dev_path = "/dev/";

    std::regex  tty_device_regex("ttyS[0-9]+$");
    std::regex  device_number_regex(R"((\d+)$)");
    std::smatch match;

    std::vector<std::string> tty_devices;

#if __cplusplus >= 201703L
    for (const auto& entry : std::filesystem::directory_iterator(dev_path)) {
        if (entry.path().filename().string().find("ttyS") == 0) {
            tty_devices.push_back(entry.path().filename().string());
        }
    }
#else
    DIR*    dir;
    dirent* ent;

    dir = opendir(dev_path);
    if (nullptr == dir) {
        std::cerr << "Failed to open " << dev_path << std::endl;
        return 1;
    }
    while ((ent = readdir(dir)) != nullptr) {
        std::string filename(ent->d_name);
        if (std::regex_match(filename, tty_device_regex)) {
            tty_devices.push_back(filename);
        }
    }

    closedir(dir);
#endif

    for (const auto& device : tty_devices) {
        if (std::regex_search(device, match, device_number_regex)
            && match.size() > 1) {
            std::cout << "Device number: " << match[1] << "\t";
        }
        std::cout << device << std::endl;
    }

    return 0;
}