#pragma once
#include <memory>
namespace my_std {

template <typename T>
class auto_ptr {
public:
    auto_ptr() : m_ptr(nullptr) {}
    auto_ptr(T* ptr) : m_ptr(ptr) {}
    ~auto_ptr() { delete m_ptr; }
    auto_ptr(auto_ptr& other) { m_ptr = other.release(); }

    auto_ptr& operator=(auto_ptr& rhs) {
        auto_ptr(rhs).swap(*this);
        return *this;
    }

    T* release() {
        T* ptr = m_ptr;
        m_ptr = nullptr;
        return ptr;
    }

    T& operator*() const { return *m_ptr; }
    T* operator->() const { return m_ptr; }
    T* get() const { return m_ptr; }

    void swap(auto_ptr& rhs) {
        using std::swap;
        swap(m_ptr, rhs.m_ptr);
    }

private:
    T* m_ptr;
};

template <typename T>
class unique_ptr {
public:
    unique_ptr() : m_ptr(nullptr) {}
    unique_ptr(T* ptr) : m_ptr(ptr) {}
    ~unique_ptr() { delete m_ptr; }

    unique_ptr(unique_ptr&& other) { m_ptr = other.release(); }

    unique_ptr& operator=(unique_ptr rhs) {
        rhs.swap(*this);
        return *this;
    }

    T& operator*() const { return *m_ptr; }
    T* operator->() const { return m_ptr; }
    T* get() const { return m_ptr; }

    T* release() {
        T* ptr = m_ptr;
        m_ptr = nullptr;
        return ptr;
    }

    void swap(unique_ptr& rhs) {
        using std::swap;
        swap(m_ptr, rhs.m_ptr);
    }

private:
    T* m_ptr;
};

template <typename T>
class shared_ptr {
private:
    class shared_count {
    public:
        shared_count() : m_count(1) {}
        ~shared_count() = default;

        void add_count() { ++m_count; }
        long reduce_count() { return --m_count; }
        long get_count() const { return m_count; }

    private:
        long m_count;
    };

public:
    explicit shared_ptr(T* ptr = nullptr)
        : m_ptr(ptr), m_shared_count(nullptr) {
        if (m_ptr) {
            m_shared_count = new shared_count;
        }
    }

    ~shared_ptr() {
        if (m_ptr && 0 == m_shared_count->reduce_count()) {
            delete m_ptr;
            delete m_shared_count;
        }
    }

    shared_ptr(shared_ptr& others) {
        m_ptr = others.m_ptr;
        if (m_ptr) {
            others.m_shared_count->add_count();
            m_shared_count = others.m_shared_count;
        }
    }

    shared_ptr(shared_ptr&& other) {
        m_ptr = other.m_ptr;
        if (m_ptr) {
            m_shared_count = other.m_shared_count;
            other.m_ptr = nullptr;
            other.m_shared_count = nullptr;
        }
    }

    shared_ptr& operator=(shared_ptr rhs) {
        // 创建形参时引用增加，this对象此时内部指针为空
        // 形参与this对象交换后，析构时已变为nullptr，不会造成引用计数减1
        rhs.swap(*this);
        return *this;
    }

    T& operator*() const { return *m_ptr; }
    T* operator->() const { return m_ptr; }
    T* get() const { return m_ptr; }

    long get_use_count() const {
        if (m_ptr)
            return m_shared_count->get_count();
        else
            return 0;
    }

    void swap(shared_ptr& rhs) {
        using std::swap;
        swap(m_ptr, rhs.m_ptr);
        swap(m_shared_count, rhs.m_shared_count);
    }

private:
    T* m_ptr;
    shared_count* m_shared_count;
};

}  // namespace my_std