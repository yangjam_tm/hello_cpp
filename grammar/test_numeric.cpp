/**
 * @brief 测试<numeric>
 */
#include <numeric>
#include <vector>
#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"
#define FMT_ONLY_HEADER
#include <fmt/format.h>

using namespace std;
using namespace fmt;

TEST_CASE("<numeric> accumulate")
{
    vector<int> v{1, 2, 3, 4, 5};

    /**
     * @fn T accumulate (InputIterator first, InputIterator last, T init);
     * @brief 对序列进行累加操作 ==> init + v[0] + ...
     */

    SUBCASE("default")
    {
        CHECK_EQ(accumulate(v.begin(), v.end(), 0), 15);

        CHECK_EQ(accumulate(v.begin(), v.end(), 30), 45);
    }

    /**
     * @fn T accumulate (InputIterator first, InputIterator last, T init, BinaryOperation binary_op);
     * @brief 迭代序列，并使用自定义的操作运算binary_op替换默认的加法
     */
    SUBCASE("binary_op")
    {
        CHECK_EQ(accumulate(v.begin(), v.end(), 1, multiplies<int>()), 120);

        CHECK_EQ(accumulate(v.begin(), v.end(), 0, minus<int>()), -15);

        auto my_op = [](int a, int b)
        { return a * 3 * b; };

        CHECK_EQ(accumulate(v.begin(), v.end(), 1, my_op), 29160);
    }
}

TEST_CASE("<numeric> adjacent_diiference")
{
    vector<int> v{1, 2, 3, 5, 8, 13, 21};

    SUBCASE("default")
    {
        vector<int> res(7);
        adjacent_difference(v.begin(), v.end(), res.begin());

        CHECK_EQ(res[0], v[0]);
        for (size_t i = 1; i < res.size(); ++i)
            CHECK_EQ(res[i], v[i] - v[i - 1]);
    }
}