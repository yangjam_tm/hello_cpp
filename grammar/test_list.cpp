#include <list>
#include <iostream>

using namespace std;


void test01()
{
    list<int> lt {1, 2, 3, 4};


    auto it1 = lt.begin();

    cout << *it1 << endl;
    
    auto it2 = it1;
    lt.erase(it1++);
    // it2++;
    cout << *it1 << endl;
    cout << *it2 << endl;
}


int main(void)
{
    test01();
}
