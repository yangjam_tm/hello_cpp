#pragma once
#include <string>

#ifdef PROJECT_DIR
// #pragma message(PROJECT_DIR)
#ifdef __linux__
const auto RESOURCES_DIR = std::string(PROJECT_DIR).append("/res/");
#elif defined(_WIN32) || defined(_WIN64)
// todo
#endif

const auto& kResourcesDir = RESOURCES_DIR;

#endif // PROJECT_DIR