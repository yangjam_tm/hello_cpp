#pragma once

#include <chrono>

namespace hello_cpp
{
class Timer
{
public:
    Timer()  = default;
    ~Timer() = default;

    void Start() { start_ = std::chrono::high_resolution_clock::now(); }
    void End() { end_ = std::chrono::high_resolution_clock::now(); }

    double AverageTime(unsigned long times = 1)
    {
        times = times == 0 ? 1 : times;
        return std::chrono::duration_cast<std::chrono::microseconds>(end_ - start_).count() / times;
    }

private:
    std::chrono::high_resolution_clock::time_point start_;
    std::chrono::high_resolution_clock::time_point end_;
};
}  // namespace hello_cpp