/**
 * @brief 矩阵算术运算
 * @author yangjian 1171267147@qq.com
 * @copyright https://eigen.tuxfamily.org/dox/group__TutorialMatrixArithmetic.html
 */
#include <eigen3/Eigen/Dense>
#include <iostream>
#include <ostream>

using namespace std;
using namespace Eigen;

int main()
{
#if 0
    Matrix2i a = Matrix2i::Random();
    Matrix2i b = Matrix2i::Random();

    // cout << "a + b = \n" << a + b << endl;
    // cout << "a - b = \n" << a - b << endl;
    // cout << "a += b a = \n" << (a += b) << endl;
    // cout << "-b = \n " << -b << endl;

    cout << "a / 1024 = \n" << a / 1024 << endl; 
    // cout << "0.01 * b = \n" << 0.01 * b << endl;    //!类型不匹配不能计算
#endif

#if 0
    VectorXi a(10), b(10), c(10), d(10);
    a = VectorXi::Constant(10, 1);
    b = VectorXi::Constant(10, 2);
    c = VectorXi::Constant(10, 3);
    // eigen在处理运算符时并不直接计算，而是返回一个运算表达式，直到“评估”(exp:=)这个变量时，才进行实际计算
    // 因此对于下面表达式，直到'='才会计算，对于3个vector只会遍历一次
    d = a + 2 * b + 3 * c;
    cout << d << endl;
#endif

#if 0
    MatrixXf m = MatrixXf::Random(2, 2);
    // 执行如下运算符时，并不立刻运算，而是返回一个代理，直到evaliated时才写到对象内存中
    // !执行a = a.transpose()时因为立刻写内存，会导致a出现混乱
    cout << "transposition m^T << \n" << m.transpose() << endl;
    // 对于实数矩阵，元素的虚部为0，因此共轭矩阵就是矩阵本身
    cout << "conjugate     m^- << \n" << m.conjugate() << endl;
    // 伴随矩阵是矩阵转置后取其共轭矩阵，因此实数矩阵的伴随就是其转置矩阵
    cout << "adjoint       m^* << \n" << m.adjoint() << endl;

    MatrixXcf complex_m = MatrixXcf::Random(2, 2);
    cout << "transposition m^T << \n" << complex_m.transpose() << endl;
    // 复数的共轭就是对其虚部取反
    cout << "conjugate     m^- << \n" << complex_m.conjugate() << endl;
    cout << "adjoint       m^* << \n" << complex_m.adjoint() << endl;

    // ==> a = a.transpose() 
    complex_m.transposeInPlace();
    cout << "transposeInPlace: \n" << complex_m << endl; 

#endif

#if 0
    MatrixXi m1 {
        {1, 2, 3},
        {3, 2, 1}
    };
    MatrixXi m2 {
        {1, 2},
        {2, 3},
        {3, 4}
    };
    Vector2i v1 = {2, 3};
    Vector3i v2 = {4, 5, 2};
    cout << "m1_2x3 * m2_3x2 = \n" << m1 * m2 << endl;
    cout << "m2 * m1 = \n" << m2 * m1 << endl;
    cout << "m1 * v2 = \n" << m1 * v2 << endl;
    cout << "v1^T * m1 = \n" << v1.transpose() * m1 << endl;
   
    Matrix2i m3 {{1, 2}, {3, 4}};
    Matrix2i m4 = m3;
    // 如上，将矩阵的计算结果赋给自己会导致"alias"问题，即内存混乱
    // 但是，eigen对乘法作为特例，会增加临时变量 tmp=m3*m3; m3=tmp;
    // 如果确定不会出现alias问题，可以使用m.noalias()禁止生成临时变量
    m3 = m3 * m3;
    cout << m3 << endl;
    // m4.noalias() = m4 * m4;
    // cout << m4 << endl;
#endif

#if 0
    Vector3i v1(1, 2, 3);
    Vector3i v2(5, 4, 3);

    cout << "dot product: \n" << v1.dot(v2) << endl;
    // cross()仅使用于三维vector，三维空间中计算两个向量的叉积
    cout << "cross product: \n" << v1.cross(v2) << endl;
    cout << "cross product: \n" << v2.cross(v1) << endl;
#endif

#if 1
    Matrix3i m;
    m = Matrix3i::Random();
    for (int i = 0; i < m.cols(); ++i)
        for (int j = 0; j < m.rows(); ++j)
            m(j, i) %= 24;
    cout << "m = \n" << m << endl;
    cout << "m.sum() = " << m.sum() << endl;
    cout << "m.prod() = " << m.prod() << endl;
    cout << "m.mean() = " << m.mean() << endl;
    cout << "m.minCoeff() = " << m.minCoeff() << endl;
    cout << "m.maxCoeff() = " << m.maxCoeff() << endl;
    cout << "m.trace() = " << m.trace() << endl;
    cout << "m.diagnol() = " << m.diagonal().transpose() << endl;

#endif

    return 0;

}
