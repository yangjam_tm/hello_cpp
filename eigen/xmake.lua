function build_case(filename)
    target(path.basename(filename))
        set_group('eigen')
        set_kind('binary')
        set_default(false)
        add_files(filename)
        add_links('pthread')
    target_end()
end

for _, file in ipairs(os.files('*.cpp')) do
    -- print("Found cpp file: " .. file)
    build_case(file)
end