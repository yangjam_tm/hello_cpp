/**
 * @brief eigen中的块操作
 * @copyright https://eigen.tuxfamily.org/dox/group__TutorialBlockOperations.html
 */

#include <eigen3/Eigen/Dense>
#include <iostream>

/**
 * matrix或array中的一个矩形区域称为块(block),block可以作为左值和右值
 * 如果允许编译器进行优化，block表达式的运行时开销为0
 * 支持的d调用形式：
 *  matrix.block(i, j, p, q);
 *  matrix.block<p, q>(i, j);
 * i,j为block在matirx的起始坐标，q,q为block大小
 * fixed和dynamic矩阵均支持block表达式
 */

using namespace std;
using namespace Eigen;

int main(void)
{
    MatrixXi m(4, 4);

    for (int i = 0; i < m.rows(); ++i)
        for (int j = 0; j < m.cols(); ++j)
            m(i, j) = rand() % 24;

    // !使用<>()表达式时'<>'内必须使用常量
    cout << m.block<2, 2>(1, 1) << endl;
    for (int i = 1; i < 3; ++i)
    {
        cout << m.block(0, 0, i, i) << endl;
    }

    Matrix2i m1         = Matrix2i::Random();
    m.block<2, 2>(1, 2) = m1;
    cout << m << endl;

    m.col(2) = Vector4i::Constant(rand() % 24 + 15);
    m.row(1) = m.col(1).transpose();
    cout << m << endl;

    cout << "topLeftCorner:: \n" << m.topLeftCorner(2, 2) << endl;
    cout << "bottomRightCorner:: \n" << m.bottomRightCorner(2, 2) << endl;

    auto m2 = m.topRows(2);
    cout << m2 << endl;

    auto v = m2.row(1);
    cout << v.head<3>() << endl;
    cout << v.tail<3>() << endl;
    v.segment(1, 2) *= 2;
    cout << "v.segment(1, 2) * 2, v = " << v << endl;

    return 0;
}
