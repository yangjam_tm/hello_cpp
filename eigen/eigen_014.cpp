/**
 * @brief eigen广播操作
 * @copyright https://eigen.tuxfamily.org/dox/group__TutorialReductionsVisitorsBroadcasting.html
 */

#include <eigen3/Eigen/Dense>
#include <iostream>

using namespace std;
using namespace Eigen;

int main(void)
{
    MatrixXi mat(2, 4);
    Vector2i vec(2);

    mat << 1, 2, 6, 9,
           3, 1, 7, 2;
    vec << 0,
           1;

    // 矩阵每一列与列向量vec相加
    // broadcast操作相当于把vec复制(广播)为2x4矩阵，其每一列由vec进行填充
    mat.colwise() += vec;

    // mat.colwise() += 2;  // error 不支持
    // colwise/rowwise 进行 +/- 操作时只能使用vector
    cout << mat << endl;

    Vector2i v2;
    v2 << 3, 4;

    Index minIndex;
    auto minDistance = (mat.colwise() - v2).colwise().squaredNorm().minCoeff(&minIndex);
    cout << "minDistance = " << minDistance << endl;
    cout << "lcoate " << minIndex << " col" << endl;

    return 0;
}
