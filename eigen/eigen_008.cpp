/**
 * @brief eigen stl操作
 * @copyright https://eigen.tuxfamily.org/dox/group__TutorialSTL.html
 */

#include <cstdlib>
#include <ctime>
#include <eigen3/Eigen/Dense>
#include <iostream>
#include <algorithm>

using namespace std;
using namespace Eigen;


int main(void)
{
    VectorXi vec = VectorXi::Random(4);

    // for(auto& x : vec)
        // cout << x << "\t";
    
    srand(static_cast<unsigned int>(time(nullptr)));
    
    auto rand_int = [](int& value){ value = random() % 1024; };

    for_each(vec.begin(), vec.end(), rand_int);
    cout << vec.transpose() << endl;

    Matrix4i mat;
    // 多维Matrix/Array不支持直接使用begin和end迭代
    // for_each(mat.begin(), mat.end(), rand_int);
    for_each(mat.reshaped().begin(), mat.reshaped().end(), rand_int);
    // 使用rowwise和colwise进行行列循环
    for (auto row : mat.rowwise())
        sort(row.begin(), row.end());
    cout << mat << endl;
    return 0;
}
