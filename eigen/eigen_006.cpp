/**
 * @brief 矩阵系数初始化
 * @copyright https://eigen.tuxfamily.org/dox/group__TutorialAdvancedInitialization.html
 */
#include <eigen3/Eigen/Dense>
#include <iostream>

using namespace std;
using namespace Eigen;

/**
 * eigen 提供逗号初始化方式来初始化matirx,array,vector的系数
 * eigen 同时允许使用matrix等通过逗号初始化的方式来初始化更大维度的矩阵系数
 * 使用逗号初始化方式时，matrix的类型必须是确定的
 * 块表达式也支持使用逗号初始化
 *
 * matrix和array支持Zero()构建0系数矩阵
 * 对于fixed类型matrix/array不需要传入参数
 * 对于dynamic类型则需要依据维度掺入不同数量的参数
 *
 * Constant(value)用于将所有系数设为同一值
 * Random()获取随机矩阵，dynamic矩阵则需要传入相应rows和cols参数
 * Identify()属于线性代数的概念，仅适用于matrix，或者通过object调用setIdentify()方法
 *
 * 对于vector或者行/列数组可以使用LinSpaced([size], low, high)来生成序列
 *
 * 当调用Zero()/Identify()的等静态方法时，并不是直接返回实际矩阵，而是返回一个表达式对象，只在evaluate时才真正计算
 */

int main(void)
{
    // 不允许在声明时使用comma初始化
    Matrix2i m1;
    m1 << 1, 2, 3, 4;
    // 使用临时对象进行逗号初始化，必须再使用finished方法转化为actual matrix object
    Matrix2i m2 = (Matrix2i() << 5, 6, 7, 8).finished();

    Matrix4i m3;
    m3 << m1, Matrix2i::Random(), Matrix2i::Random(), m2;
    cout << m3 << endl;

    MatrixXi m4(6, 6);
    m4.topLeftCorner(2, 2)  = MatrixXi::Zero(2, 2);
    m4.block(0, 2, 2, 2)    = MatrixXi::Constant(2, 2, 123);
    m4.topRightCorner(2, 2) = MatrixXi::Identity(2, 2);
    m4.row(2)               = RowVectorXi::Zero(m4.rows());
    m4.row(3)               = RowVectorXi::LinSpaced(6, 1, 10);
    // m4(seq(0,1), lastN(2)).setIdentify();  // block没有setIdentify方法
    m4.bottomLeftCorner(2, 2).setIdentity();
    m4(lastN(2), seq(2, 3)).setRandom();
    m4.bottomRightCorner(2, 2).setZero();
    cout << m4 << endl;

    return 0;
}
