/**
 * @brief 切片
 * @copyright
 * https://eigen.tuxfamily.org/dox/group__TutorialSlicingIndexing.html
 */

#include <eigen3/Eigen/Dense>
#include <iostream>

using namespace std;
using namespace Eigen;

int main(void) 
{
    MatrixXf m = MatrixXf::Random(8, 8);
    
    // seq(1, 3) ==> [1, 2, 3] 
    // 读取第1,2,3列的所有行
    cout << "(all, seq(1, 3))\n" << m(Eigen::all, seq(1, 3)) << endl; 
    // seqN(1,3) ==> [1, 2, 3] 
    // seq(1, 4, 2) ==> [1, 3]
    // 读取第1,3行的第1,2,3列
    cout << "(seq(1, 4, 2), seqN(1, 3))\n" << m(seq(1, 4, 2), seqN(1, 3)) << endl;
    // seqN(1, 3, 2) ==> [1, 3, 5]
    cout << "(seqN(1, 3, 2), seqN(1, 3, 2))\n" << m(seqN(1,3,2), seqN(1, 3, 2)) << endl;
    // ?fix<int>强制编译时使用编译时参数，用于优化性能
    // 当等差数据使用负数间距时，表示倒序排列
    cout << "(seqN(5,3,fix(-2)), ...)\n" << m(seqN(5, 3, fix<-2>), seqN(5, 3, fix<-2>)) << endl;

    auto v = m.row(4);
    cout << "lastN(4) = " << v(lastN(4)) << endl;
    cout << "m(lastN(3), lastN(3)) = " << m(lastN(3), lastN(3)) << endl;
    return 0;
}
