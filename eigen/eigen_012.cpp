#include <eigen3/Eigen/Dense>
#include <iostream>

using namespace Eigen;
using namespace std;

// array与matrix组织形式相同

int main(void)
{
#if 0
    // 定义一维列数组
    ArrayXf a1 = ArrayXf::Random(5);
    // 定义二维数组
    ArrayXXf a2 = ArrayXXf::Random(3, 2);
    
    cout << a1 << "\n" << a2 << endl;
#endif

    MatrixXi m1;
    MatrixXi m2;
    // !不能用逗号初始化来初始化一个尺寸为0x0(未初始化)的矩阵
    m1.resize(2, 2);
    m2.resize(2, 2);
    m1 << 1, 2, 3, 4;
    m2 << 5, 6, 7, 8;
    // matirx乘法遵循矩阵乘法规则
    cout << "m1 * m2 = " << m1 * m2 << endl;

    ArrayXXi a1(2, 2);
    ArrayXXi a2(2, 2);
    a1 << 1, 2, 3, 4;
    a2 << 5, 6, 7, 8;
    // array乘法为对应位置元素相乘
    cout << "a1 * a2 = " << a1 * a2 << endl;

    // matrix转array
    cout << "m1.array() * m2.array() = " << m1.array() * m2.array() << endl;
    // array转matrix
    cout << "a1.matrix() * a2.matrix() = " << a1.matrix() * a2.matrix() << endl;
    // matirx按array相乘
    cout << "cwiseProduct: " << m1.cwiseProduct(m2) << endl;
}
