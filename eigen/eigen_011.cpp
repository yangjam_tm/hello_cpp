#include <eigen3/Eigen/Dense>
#include <iostream>

// X表示维度不定，d表示数据为double
using Eigen::MatrixXd;
using Eigen::VectorXd;
using Eigen::Matrix3d;
using Eigen::Vector3d;
using namespace std;

int main(void)
{
    // 创建一个2*2的双精度矩阵
    MatrixXd m(2, 2);
    // 为每个元素单独赋值
    m(0, 0) = 3;
    m(1, 0) = 2.5;
    m(0, 1) = -1;
    m(1, 1) = m(1, 0) + m(0, 1);
    std::cout << m << std::endl;
    
    // 创建3*3，每个元素在-1~1之间的随机矩阵
    MatrixXd m1 = MatrixXd::Random(3, 3);
    m1 = (m1 + MatrixXd::Constant(3, 3, 1.2)) * 50;
    cout << m1 << endl;

    // 创建1*3的行向量
    VectorXd v1(3);
    v1 << 1, 2, 3;
    cout << v1 << endl;
    cout << "m1 * v1 = " << m1 * v1 << endl;
    
    // 相较于运行时确定大小，编译时确定大小，优点在于
    // 1. 编译更快
    // 2. 有利于编译时检查错误
    Matrix3d m2 = Matrix3d::Random();
    m2 = (m2 + Matrix3d::Constant(1.2)) * 20;
    cout << m2 << endl;

    Vector3d v2 {1.2, 1.3, 1.4};
    cout << "m2 * v2 = " << m2 * v2 << endl;

    return 0;
}
