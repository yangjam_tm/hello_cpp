/**
 * @brief Eigen_Matrix基础使用
 * @author yangjian 1171267147@qq.com
 * @copyright https://eigen.tuxfamily.org/dox/group__TutorialMatrixClass.html
 */
#include <eigen3/Eigen/Dense>
#include <iostream>

using namespace Eigen;
using namespace std;

// Eigen中matrix和vector均是Matirx模板的特例实现
// Matirx<typename Scalar, int RowsAtCompileTime, int ColsAtCompileTime，
//      int Options = 0，
//      int MaxRowsAtCompileTime = RowsAtCompileTime,
//      int MaxColsAtCompileTime = ColsAtCompileTime>
// 当Row/Col_AtCompileTime = Dynamic时，就是动态矩阵
// Options可以控制矩阵的存储顺序，当Options=RowMajor，矩阵采用行顺序存储
// Max参数用于控制矩阵的最大分配内存，当数值不大时可以避免动态内存分配

/**
 * 当矩阵尺寸不超过16时，推荐使用fixed矩阵，此时矩阵相当于一个array[16]，没有任何运行是开销
 * 当矩阵尺寸大于32时，fixed相较于dynamic就失去了性能优势，而且尺寸太大可能导致栈溢出，eigen会将其分配在堆上
 */

int main(void)
{
    // 默认构造，但是不会分配动态内存，不会初始化系数(矩阵元素)
    // MatrixXf a;
    // VectorXf b;
    
    // 默认构造，相当于float c[3]，不会初始化系数
    // Vector3f c;

#if 0
    // c++11之后，eigwn支持任意维度矩阵的带系数初始化语法
    Vector2i a(1, 2);
    
    Matrix<int, 1, 5> b {1, 2, 3, 4, 5};

    MatrixXi c {
        {1, 2, 3},
        {4, 5, 6}
    };
    
    MatrixXf d(2, 3);
    d << 1.2, 2.3,
         2.4, 2.5,
         3.6, 3.7;

    cout << a << endl;
    cout << b << endl;
    cout << c << endl;
    cout << d << endl;
#endif
    
#if 0
    // !Eigen矩阵默认按列进行存储，c++ []不支持多个参数，所以使用()进行元素访问
    MatrixXf m = MatrixXf::Random(3, 2);
    cout << m << endl;
    cout << m(0, 0) << '\t' << m(0, 1) << endl;

    // Vector3i v << 1, 2, 3;  // 不支持在初始化时同时使用逗号初始化
    // Eigen对Vector重载了[]
    Vector3i v(1, 2, 3);
    cout << v[1] << '\t' << v[2] << endl;
#endif

#if 1
    MatrixXi m(2, 3);
    m = MatrixXi::Random(2, 3);
    cout << m << endl;

    m.resize(2, 3);    // size没变，不会操作矩阵
    m.resize(3, 2);
    cout << m << endl;

    MatrixXi mt = m;
    m.resize(3, 3);    // size改变，会修改原来的系数
    cout << m << endl;
    mt.conservativeResize(3, 3);   // resize同时保留原有系数
    cout << mt << endl;

    Vector3i v {1, 2, 3};
    v.resize(3);   // size没变，不发生实际操作，
    // v.resize(5);   // 固定(fixed)尺寸的矩阵改变尺寸运行报错
    cout << v << endl;
#endif

    return 0;
}
