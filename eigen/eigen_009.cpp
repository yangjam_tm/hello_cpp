/**
 * @brief eigen reshape
 * @copyright https://eigen.tuxfamily.org/dox/group__TutorialReshape.html
 */

#include <eigen3/Eigen/Dense>
#include <iostream>

using namespace Eigen;
using namespace std;

int main(void)
{
    Matrix4i mat = Matrix4i::Random();

    // reshape并不是对mat进行inplace变形，而是返回一个view
    // view与原矩阵共享内存
    cout << mat.reshaped(2, 8) << endl;
    cout << mat << endl;
    // 获取矩阵的1d视图，并使用等差数列进行填充
    mat.reshaped() = VectorXi::LinSpaced(16, 0, 16);
    cout << mat << endl;
    // 获取矩阵按行排列的1d视图，并转置为行向量
    cout << mat.reshaped<RowMajor>().transpose() << endl;

    // mat.resize(2, 8);   // 固定矩阵不支持resize, runtime_error
    MatrixXi mat2 = mat;
    // 矩阵进行就地重排，改变矩阵原来尺寸
    mat2.resize(2, 8);
    cout << mat2 << endl;
    return 0;
}
