/**
 * @brief eigen 数据压缩
 * @copyright https://eigen.tuxfamily.org/dox/group__TutorialReductionsVisitorsBroadcasting.html
 */

#include <eigen3/Eigen/Dense>
#define FMT_HEADER_ONLY
#include <fmt/core.h>
#include <fmt/format.h>
#include <iostream>

using namespace Eigen;
using namespace std;

int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{
    Matrix2d mat;
    mat << 1, 2, 3, 4;
    
    // 计算所有系数的和
    fmt::print("mat.sum() = {}\n", mat.sum());
    // 计算所有系数的积
    fmt::print("mat.prod() = {}\n", mat.prod());
    // 计算所有系数的平均值
    fmt::print("mat.mean() = {}\n", mat.mean());
    // 最小系数
    fmt::print("mat.minCoeff() = {}\n", mat.minCoeff());
    fmt::print("mat.maxCoeff() = {}\n", mat.maxCoeff());
    // 计算对角线系数和
    fmt::print("mat.trace() = {}\n", mat.trace());

    // 矩阵范数
    Vector3f v {-1, 2, 3};
    MatrixXf m = (MatrixXf(2, 2) << 1, -2, -3, 4).finished();

    // 平方范数，所有矩阵系数的平方和
    fmt::print("v.squaredNorm() = {}\n", v.squaredNorm());
    // F范数，平方范数的平方根，也称为L2范数
    fmt::print("v.norm() = {}\n", v.norm());
    // ---- Lp范数 ---- 
    // L1范数，系数绝对值之和
    fmt::print("v.lp<1>Norm() = {}\n", v.lpNorm<1>());
    fmt::print("v.lp<2>Norm() = {}\n", v.lpNorm<2>());
    fmt::print("v.lp<Infinity>Norm() = {}\n", v.lpNorm<Infinity>());


    fmt::print("m.squaredNorm() = {}\n", m.squaredNorm());
    fmt::print("m.norm() = {}\n", m.norm());
    fmt::print("m.lp<1>Norm() = {}\n", m.lpNorm<1>());
    fmt::print("m.lp<2>Norm() = {}\n", m.lpNorm<2>());
    // 无穷范数，系数绝对值的最大值
    fmt::print("m.lp<Infinity>Norm() = {}\n", m.lpNorm<Infinity>());


    // !与标量进行比较仅Array类型支持
    // 判断m中是否所有元素都大于0
    fmt::print("(m > 0).all() = {}\n", (m.array() > 0).all());
    // 判断m所有元素中是否存在大于0的元素
    fmt::print("(m > 0).any() = {}\n", (m.array() > 0).any());
    // 统计m中大于2的元素数量
    fmt::print("(m > 2).count() = {}\n", (m.array() > 2).count());

    // m = m + 4;    // matrix不支持直接与标量相加
    m = (m.array() + 4).matrix();
    // cout << "m + 4 = " << m << endl;
    // m = m % 24;   // matrix不支持直接%一个标量
    // Array3i a3x3 = Array3i::Random() % 24;    // array也不支持%

    return 0;
}
