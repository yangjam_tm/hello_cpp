/**
 * @brief 通过指针创建矩阵
 * @copyright https://eigen.tuxfamily.org/dox/group__TutorialMapClass.html
 */

#include <cstdlib>
#include <eigen3/Eigen/Dense>
#include <iostream>
#include <array>
#include <algorithm>

using namespace std;
using namespace Eigen;

int main(void)
{
    array<int, 8> arr;
    
    srand(static_cast<unsigned>(time(nullptr)));
    auto rand_int = [](int& val){ val = rand() % 24; };

    for_each(arr.begin(), arr.end(), rand_int);

    Map<MatrixXi> mInt(&arr[0], 2, 4);
    cout << mInt << endl;

    auto mat2 = Map<Matrix<int, 4, 2, RowMajor>>(arr.data());
    cout << mat2 << endl;

    return 0;

}
