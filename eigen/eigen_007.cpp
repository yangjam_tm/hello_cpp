/**
 * @brief eigen中的aliasing问题
 * @copyright https://eigen.tuxfamily.org/dox/group__TopicAliasing.html
 */

#define EIGEN_NO_DEBUG
#include <eigen3/Eigen/Dense>
#include <iostream>

using namespace std;
using namespace Eigen;

/**
 * 当同一个矩阵出现在 = 两侧时就可能出现alias问题
 * 可以简单理解为，eigen为了性能，并不会创建临时变量，而是在同一块内存上进行操作
 * 如果 = 两侧是同一块内存，一块内存如果存在多次不同的操作，就可能出现结果不符合预期的问题
 */

int main(void)
{
    Matrix4i mat;
    mat.reshaped() = VectorXi::LinSpaced(16, 0, 16);

    // 每一系数的内存只操作一次，不会出现alias问题
    mat = 2 * mat;
    cout << mat << endl;

    // 转置时，先将第一行赋给第一列，此时原矩阵的第一列已经被覆盖，再使用第一列时结果就出现alias问题
    // mat = mat.transpose();
    // cout << mat << endl;

    // 使用eval方法在右侧创建一个临时变量，计算完成后再赋给左侧，用于解决alias问题
    mat = mat.transpose().eval();
    cout << mat << endl;

    return 0;
}
