/**
 * @brief eigen访问矩阵元素
 * @copyright https://eigen.tuxfamily.org/dox/group__TutorialReductionsVisitorsBroadcasting.html
 */
#include <eigen3/Eigen/Dense>
#define FMT_HEADER_ONLY
#include <fmt/core.h>
#include <fmt/format.h>
#include <iostream>

using namespace Eigen;
using namespace fmt;
using namespace std;

int main(void)
{
    MatrixXi mat = MatrixXi::Random(3, 3);

    Index minRow, minCol;
    Index maxRow, maxCol;

    // 查找矩阵极值并获取其行列序号
    auto minCoeff = mat.minCoeff(&minRow, &minCol);
    auto maxCoeff = mat.maxCoeff(&maxRow, &maxCol);

    print("min coeff at: ({} {}) is {}\n", minRow, minCol, minCoeff);
    print("max coeff at: ({} {}) is {}\n", maxRow, maxCol, maxCoeff);

    // 对mat的每一列执行maxCoeff方法
    cout << mat.colwise().maxCoeff() << endl;
    cout << mat.rowwise().minCoeff() << endl;

    Index maxIndex;
    auto maxNorm = mat.colwise().sum().maxCoeff(&maxIndex);
    print("maxNorm = {} locate in {}rd col\n", maxNorm, maxIndex);

    return 0;
}

