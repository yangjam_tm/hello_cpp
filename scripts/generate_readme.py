#!/usr/bin/env python3
"""为当前目录下的cpp文件生成readme文件"""
import re
import os
import sys

def read_brief(filename):
    """
    从文件注释中读取brief的内容
    """

    # 打开文件，并读取文件的前5行
    with open(filename, 'r') as file:
        lines = file.readlines()[:5]

    # 从lines中读取包含 "@brief" 的行
    brief_line = [line for line in lines if '@brief' in line]
    if brief_line == []:
        return f'{filename}没有注释'

    # 从brief_line中提取brief的内容
    brief_content = brief_line[0].split('@brief')[1].strip()

    return brief_content


def create_link(filename, content):
    '''按markdown语法创建文件链接'''
    str = f'- [{content}]({filename})'
    return str

if __name__ == '__main__':

    # 如果传递了目录参数，就切换到对应目录执行
    if len(sys.argv) > 1:
        dir = sys.argv[1]
        os.chdir(dir)

    # 遍历当前目录下所有cpp文件并排序
    files = [f for f in os.listdir('.') if f.endswith('.cpp')]
    files.sort()

    # 遍历所有cpp文件，生成对应的markdown链接
    links = [create_link(f, read_brief(f)) for f in files]

    # 将readme清空，然后将links写入到readme文件
    with open('readme.md', 'r+') as f:
        f.write('\n'.join(links))

    # 打印文件内容
    with open('readme.md', 'r') as f:
        print(f.read())
