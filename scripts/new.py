#!/usr/bin/env python3

import sys
import os


def get_files_sum(dir):
    '''获取指定目录下，符合条件的文件数量
    '''
    count = 0
    for _, _, files in os.walk(dir):
        for file in files:
            if file.endswith('.cpp'):
                count += 1
    return count


if __name__ == '__main__':
    # 读取命令行参数

    if len(sys.argv) != 2:
        print("Usage: ./new.py <dir>")
        sys.exit(1)

    dir = sys.argv[1]
    # 遍历目录下的所有文件
    count = get_files_sum(dir)
    # print(f"目录 {dir} 下的文件数量为 {count}")
    new_file = f'{dir}_{count+1:03d}.cpp'
    # 在dir目录下创建new_file
    print(f'Creating {dir}/{new_file}')
    os.system(f'touch {dir}/{new_file}')
    os.system('xmake project -k compile_commands')
