#!/usr/bin/env python3
"""将目录下所有cpp文件按规则统一重名"""
import os
from datetime import datetime

if __name__ == "__main__":

    # 获取当前目录不带路径的名称
    current_dir = os.path.basename(os.getcwd())

    files = [file for file in os.listdir(".") if file.endswith(".cpp")]
    files.sort(key=lambda x: os.path.getmtime(x))
    for index, filename in enumerate(files, start=1):
        new_name = f"{current_dir}_{index:03d}.cpp"
        os.rename(filename, new_name)
        print(f"Renaming {filename} to {new_name}")
