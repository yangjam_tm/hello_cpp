c++代码练习仓库

* include: 测试代码依赖的头文件
* lang: [c++语法测试和常用代码片段](lang/readme.md)

## 操作命令

* `git submodule update --init --recursive` 更新子模块，将其checkout到本地
* `xmake f -k shared -P .; xmake -P .` 将当前目录作为xmake编译工程，并指定编译为动态库
* `xmake project -k compile_commands` 创建clangd编译命令文件
