/**
 * @brief TCP服务器通信基本流程
 * @copyright https://cppguide.cn/pages/299979/
 */

#include <arpa/inet.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include <exception>
#include <iostream>

using namespace std;

int main(int argc, char** argv)
{
    int flag = 0;

    try {
        // 1. 创建一个侦听socket
        auto listenFd = socket(AF_INET, SOCK_STREAM, 0);
        if (-1 == listenFd) throw runtime_error("create listen socket error!");

        // 2. 初始化服务器地址
        sockaddr_in bindAddr;
        bindAddr.sin_family      = AF_INET;
        bindAddr.sin_addr.s_addr = htonl(INADDR_ANY);
        bindAddr.sin_port        = htons(8080);

        if (-1 == bind(listenFd, (sockaddr*)&bindAddr, sizeof(bindAddr)))
            throw runtime_error("bind listen socket error!");

        // 3. 启动监听
        if (-1 == listen(listenFd, SOMAXCONN))
            throw runtime_error("listen error!");

        while (true) {
            sockaddr_in clientAddr;
            socklen_t   clientAddrLen = sizeof(clientAddr);

            // 4. 接受客户端连接
            auto clientFd =
                accept(listenFd, (sockaddr*)&clientAddr, &clientAddrLen);
            if (-1 != clientFd) {
                char recvBuf[32] = {0};

                // 5. 接收客户端数据
                auto ret = recv(clientFd, recvBuf, sizeof(recvBuf), 0);
                if (ret > 0) {
                    cout << "recv data from client: " << recvBuf << endl;

                    // 6. 发送数据给客户端
                    ret = send(clientFd, recvBuf, strlen(recvBuf), 0);
                    if (strlen(recvBuf) != ret)
                        cout << "send data to client error!" << endl;
                    else
                        cout << "send data to client success!" << endl;
                } else
                    cout << "recv data from client error!" << endl;

                close(clientFd);
            }
        }

        // 7. 关闭侦听socket
        close(listenFd);

    } catch (const std::exception& e) {
        std::cerr << e.what() << '\n';
        flag = -1;
    }

    return flag;
}