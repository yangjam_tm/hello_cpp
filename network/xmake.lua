function build_case(filename)
    target(path.basename(filename))
        set_group('network')
        set_kind('binary')
        add_files(filename)
        -- set_targetdir('./')
        set_default(false)
    target_end()
end

for _, file in ipairs(os.files('*.cpp')) do
    -- print("Found cpp file: " .. file)
    build_case(file)
end