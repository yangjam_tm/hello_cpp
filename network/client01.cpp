/**
 * @brief TCP服务器通信基本流程
 * @copyright https://cppguide.cn/pages/299979/
 */

#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include <cstring>
#include <exception>
#include <iostream>
// #include <string>

using namespace std;

int main(int argc, char* argv[])
{
    int flag = 0;

    try {
        // 1. 创建客户端套接字
        auto clientFd = socket(AF_INET, SOCK_STREAM, 0);
        if (-1 == clientFd)
            throw std::runtime_error("create client socket error");

        // 2. 连接服务器
        sockaddr_in serverAddr;
        serverAddr.sin_family      = AF_INET;
        serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
        serverAddr.sin_port        = htons(8080);

        if (-1 == connect(clientFd, (sockaddr*)&serverAddr, sizeof(serverAddr)))
            throw std::runtime_error("connect server error");

        // 3. 发送数据
        char buf[32] = {"Hello TCP!"};

        auto ret = send(clientFd, buf, strlen(buf), 0);
        if (strlen(buf) != ret) throw std::runtime_error("send data error");

        cout << "send data success" << endl;

        // 4. 接收数据
        bzero(buf, sizeof(buf));
        ret = recv(clientFd, buf, sizeof(buf), 0);
        if (ret < 0)
            throw std::runtime_error("recv data error");
        else if (ret > 0)
            cout << "recv data: " << buf << endl;

        // 5. 关闭套接字
        close(clientFd);

    } catch (const std::exception& e) {
        std::cerr << e.what() << '\n';
        flag = -1;
    }

    return flag;
}