#include <dlib/gui_widgets.h>
#include <dlib/image_transforms.h>
#include <cmath>

using namespace dlib;
using namespace std;

int main(void)
{
    std::vector<perspective_window::overlay_dot> points;
    dlib::rand rnd;

    for (double i = 0; i < 20; i += 0.01)
    {
        // 生成3维空间的正弦函数点集
        dlib::vector<double> val(sin(i), cos(i), i / 4);
        
        // 利用随机数生成rgb值并归一化到[0,20]范围内进行颜色映射
        dlib::vector<double> temp(rnd.get_random_gaussian(),
                rnd.get_random_gaussian(),
                rnd.get_random_gaussian());
        val += temp / 20;
        rgb_pixel color = colormap_jet(i, 0, 20);

        points.push_back(perspective_window::overlay_dot(val, color));
    }
    
    perspective_window win;
    win.set_title("perspective_window 3D point cloud");
    win.add_overlay(points);
    win.wait_until_closed();
    return 0;
}
