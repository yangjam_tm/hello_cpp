#!/usr/bin/bash
build_file=""
if [[ -z $1 ]]&&[[ -e $1 ]]
then 
    build_file=$1
else
    build_file=$(ls -t | grep -E ".c|.cpp" | head -n 1)
fi
if [ -e ${build_file} ]
then
    echo "build ${build_file}"
    make obj=${build_file}
fi
run_file=${build_file%.*}
if [ -e ${run_file} ]
then
    echo "------ run ${run_file} ------"
    ./${run_file}; rm ${run_file}
fi
