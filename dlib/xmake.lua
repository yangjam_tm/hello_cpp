
add_requires('pkgconfig::dlib-1', {alias = "dlib"})

function build_case(filename)
    target(path.basename(filename))
        set_group('dlib')
        set_kind('binary')
        add_files(filename)
        set_default(false)
        -- set_targetdir('./')
        add_packages('dlib')
    target_end()
end

for _, file in ipairs(os.files('*.cpp')) do
    -- print("Found cpp file: " .. file)
    build_case(file)
end
