-- 在pugiconfig.hpp中启用 `#define PUGIXML_HEADER_ONLY` 使能pugixml的仅包含头文件特性
target("pugixml")
    set_kind("static")
    add_files("*.cpp")
target_end()