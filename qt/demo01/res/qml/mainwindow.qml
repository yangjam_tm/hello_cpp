import QtQuick
import QtQuick.Window
import QtQuick.Controls

// 创建一个窗口
Window {
    id: root
    visible: true
    width: 800
    height: 600
    maximumWidth: 1280
    maximumHeight: 720
    title: qsTr("Hello World")

    // 使用一个Compnent定义一个组件，方便复用
    Component {
        id: widgetWithAnimation

        Rectangle {
            id: defaultWidget
            width: 80
            height: 80

            // 添加一个自定义属性，用于传递背景图片
            property string background: ""
            property string title: ""

            Image {
                anchors.fill: parent
                source: background
                fillMode: Image.PreserveAspectFit
            }

            Text {
                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr(title)
                font.family: "LXGW WenKai Mono"
            }

            PropertyAnimation {
                id: enterAnimation
                target: defaultWidget
                properties: "width, height"
                to: 100
                duration: 500
                running: false
            }

            PropertyAnimation {
                id: exitAnimation
                target: defaultWidget
                properties: "width, height"
                to: 80
                duration: 500
                running: false
            }

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true

                onEntered: {
                    enterAnimation.running = true;
                }

                onExited: {
                    exitAnimation.running = true;
                }
            }
        }
    }

    Row {
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.rightMargin: 2
        anchors.topMargin: 2
        spacing: 4
        height: 50

        Button {
            id: minButton
            // text: qsTr("min")
            // backgorund继承自Image，相当于用Image去作为button的背景填充
            background: Image {
                source: minButton.hovered ? "min2.png" : "min.png"
                // Stretch 拉伸填充
                // PreserveAspectFit 按比例缩放不裁剪
                // PreserveAspectCrop 按比例缩放,如有必要进行裁剪
                // Tile 水平和竖直方向复制
                // TileVertical 竖直方向复制
                // TileHorizontal 水平方向复制
                // Pad 不作变换
                fillMode: Image.PreserveAspectFit
            }
        }

        Button {
            id: maxButton
            // text: qsTr("max")
            background: Image {
                source: maxButton.hovered ? "max2.png" : "max.png"
                fillMode: Image.PreserveAspectFit
            }
            onClicked: {
                showFullScreen();
            }
        }

        Button {
            id: closeButton
            // text: qsTr("close")
            // icon.source: "close.png"
            background: Image {
                source: closeButton.hovered ? "close2.png" : "close.png"
                fillMode: Image.PreserveAspectFit
            }
            onClicked: {
                root.close();
            }
        }
    }

    Row {
        anchors.centerIn: parent
        width: 400
        height: 100
        spacing: 20

        // 通过Loader加载组件
        Loader {
            id: homeWidget
            sourceComponent: widgetWithAnimation
            onLoaded: {
                item.background = "home.png";
                item.title = "主页";
            }
        }

        Loader {
            id: contactWidget
            sourceComponent: widgetWithAnimation
            onLoaded: {
                item.background = "contact.png";
                item.title = "联系人";
            }
        }

        Loader {
            id: imgWidget
            sourceComponent: widgetWithAnimation
            onLoaded: {
                item.background = "img.png";
                item.title = "相册";
            }
        }
        Loader {
            id: recordWidget
            sourceComponent: widgetWithAnimation
            onLoaded: {
                item.background = "record.png";
                item.title = "录音";
            }
        }
    }
}
