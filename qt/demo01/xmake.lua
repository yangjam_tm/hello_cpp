target('qt_demo01')
    set_default(false)
    set_group('qt')
    set_languages('c++17')  -- Qt6.7 要求>=c++17
    add_rules('qt.quickapp')
    add_files('res.qrc')
    add_files('*.cpp')
target_end()