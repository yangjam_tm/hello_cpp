import QtQuick
import QtQuick.Window
import QtQuick.Controls

// 创建一个窗口
Window {
    id: root
    visible: true
    width: 800
    height: 600
    title: qsTr("Hello World")

    // 给窗口中添加一个按钮
    Button {
        text: qsTr("Hello World")
        anchors.centerIn: parent
        // 按下按钮，窗口背景切换一个随机颜色
        onClicked: {
            root.color = Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
        }
    }
}