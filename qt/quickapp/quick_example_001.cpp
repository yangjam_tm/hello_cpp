/**
 * @file quick_example_001.cpp
 * @author 逆流 (1171267147@qq.com)
 * @brief quickapp demo 测试
 * @version 0.1
 * @date 2024-07-05
 *
 * @copyright Copyright (c) 2024.
 *
 */

#include <QGuiApplication>
#include <QQmlApplicationEngine>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    engine.load(QUrl(QStringLiteral("qrc:/example_001.qml")));

    if (engine.rootObjects().isEmpty()) return EXIT_FAILURE;

    return app.exec();
}