#include <QCoreApplication>
#include <QThreadPool>
#include <iostream>

using namespace std;

///************************************************************************
/// QThreadPool使用步骤
/// 1. 定义一个 QRunable 子类，重写 run() 函数
/// 2. 将子类对象传递给 QThreadPool::start()
///************************************************************************

class SleepTask : public QRunnable
{
    void run() override
    {
        cout << "sleep 1s" << endl;
        QThread::sleep(1);
        cout << "sleep 1s done" << endl;
    }

    // QRunnable对象默认会SetAutoDelete标志，任务执行完后，会自动调用析构函数
    ~SleepTask() { cout << "~SleepTask()" << endl; }
};

class TimeTask : public QRunnable
{
public:
    TimeTask() : index_(kIndex++) {}
    void run() override
    {
        int count = 10;
        while (count-- > 0) {
            auto t = rand() % 10;
            cout << index_ << "---time task: " << t << endl;
            QThread::sleep(t);
        }
    }
    static int kIndex;

private:
    int index_;
};
int TimeTask::kIndex = 0;

int main(int argc, char** argv)
{
    QCoreApplication app(argc, argv);

    auto pool = QThreadPool::globalInstance();

    // 查询可用最大线程数量
    cout << "max threads: " << pool->maxThreadCount() << endl;
    cout << "max threads:" << QThread::idealThreadCount() << endl;
    // 设置线程池中可用的最大线程数量
    pool->setMaxThreadCount(pool->maxThreadCount());

    // 将任务加入到线程池并启动，任务执行完后，pool会自动删除QRunable对象
    pool->start(new SleepTask);
    // 查询当前运行线程数
    cout << "current run threads:" << pool->activeThreadCount() << endl;
    // 设置线程超时等待，如果等待事件内，所有线程都被移除，则返回true；-1表示忽略超时，等待最后一个线程退出
    pool->waitForDone(-1);

    cout << "current run threads:" << pool->activeThreadCount() << endl;

    pool->start(new TimeTask);
    pool->start(new TimeTask);
    pool->start(new TimeTask);
    pool->start(new TimeTask);

    pool->waitForDone(-1);

    return app.exec();
}