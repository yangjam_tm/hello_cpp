function build_console_example(filename)
    target(path.basename(filename))
    set_default(false)
    set_group('qt')
    set_languages('c++17')  -- Qt6.7 要求>=c++17
    add_rules('qt.console')
    add_files(filename)
    target_end()
end

for _, file in ipairs(os.files('*.cpp')) do
    build_console_example(file)
end
