
add_requires('pkgconfig::gtest_main', {alias = "gtest_main"})
add_requires('pkgconfig::gmock_main', {alias = "gmock_main"})

function build_test(filename)
    target(path.basename(filename))
        set_kind("binary")
        set_default(false)
        add_files(filename)
        -- add_deps("pugixml")
    target_end()
end

function build_test_with_gtest(filename, packname)
    packname = packname or "gtest_main"  -- packname为空时默认使用gtest_main
    target(path.basename(filename))
        set_kind("binary")
        set_default(false)
        add_files(filename)
        add_packages(packname)
    target_end()
end

-- for _, file in ipairs(os.files('*.cpp')) do
build_test('pugixml_test01.cpp')
build_test('cmdline_test01.cpp')
build_test_with_gtest('gtest01.cpp')
-- end