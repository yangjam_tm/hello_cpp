/**
 * @file cmdline_test01.cpp
 * @author 逆流 (1171267147@qq.com)
 * @brief 测试cmdline
 * @version 0.1
 * @date 2024-08-05
 *
 * @copyright Copyright (c) 2024.
 *
 */
#include "cmdline/cmdline.h"

int main(int argc, char **argv) {
    cmdline::parser a;

    // add specified type of variable.
    // 1st argument is long name
    // 2nd argument is short name (no short name if '\0' specified)
    // 3rd argument is description
    // 4th argument is mandatory (optional. default is false)
    // 5th argument is default value  (optional. it used when mandatory is false)
    a.add<std::string>("modify", 'm', "modify attribute", false, "");
    a.add<std::string>("insert", 'i', "insert attribute", false, "");
    a.add<std::string>("add", 'a', "add node", false, "");

    std::cout << a.usage() << std::endl;

    a.parse_check(argc, argv);

    if (a.exist("modify")) {
        std::cout << "modify: " << a.get<std::string>("modify") << std::endl;
    }

    if (a.exist("insert")) {
        std::cout << "insert: " << a.get<std::string>("insert") << std::endl;
    }

    if (a.exist("add")) {
        std::cout << "add: " << a.get<std::string>("add") << std::endl;
    }

    return 0;
}
