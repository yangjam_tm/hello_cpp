/**
 * @file gtest01.cpp
 * @author 逆流 (1171267147@qq.com)
 * @brief 测试gtest基本框架
 * @version 0.1
 * @date 2024-08-22
 *
 * @copyright Copyright (c) 2024.
 *
 */
#include <gtest/gtest.h>

TEST(HelloTest, BasicAssertions) { EXPECT_EQ(7 * 6, 42); }
