/**
 * @file pugixml_test01.cpp
 * @author 逆流 (1171267147@qq.com)
 * @brief pugixml xpath接口测试
 * @version 0.1
 * @date 2024-08-02
 *
 * @copyright Copyright (c) 2024.
 *
 */

#include <iostream>
#include <string>
#include "pugixml/pugixml.hpp"
#include "env.h"

int main() {
    pugi::xml_document doc;
    std::string        file = kResourcesDir + "DevAlarmCfg.xml";

    auto result = doc.load_file(file.c_str());
    if (!result) {
        std::cout << "load file failed" << std::endl;
        return 0;
    }

    // 通过xpath获取指定节点
    pugi::xpath_node node = doc.select_node("/TerminalAlarmIdCfg/Base/VerNO");
    if (node) {
        std::cout << "node name: " << node.node().name() << std::endl;
        // 遍历节点属性
        for (auto attr : node.node().attributes()) {
            std::cout << "attr name: " << attr.name() << " attr value: " << attr.value() << std::endl;
        }
    } else {
        std::cout << "node not found" << std::endl;
    }

    // pugixml支持通过xpath格式直接访问属性节点
    pugi::xpath_node attr = doc.select_node("/TerminalAlarmIdCfg/Base/VerNO[@Value]");
    if (attr) {
        std::cout << "attr name: " << attr.node().name() << std::endl;
        std::cout << "attr value: " << attr.node().first_attribute().value() << std::endl;
    }

    pugi::xpath_node arrayAttr = doc.select_node("/TerminalAlarmIdCfg/AlarmIdInfo/Alarm[2][@Id]");
    if (arrayAttr) {
        std::cout << "arrayAttr name: " << arrayAttr.node().name() << std::endl;
        std::cout << "arrayAttr value: " << arrayAttr.node().first_attribute().value() << std::endl;
    }

    return 0;
}
