-- 设置主工程

set_project('hello_cpp')
add_rules('mode.debug', 'mode.release')


-- 设置语言规范
set_languages('c++14')
set_toolchains('clang')
set_warnings('all', 'error')
if is_mode('debug') then
    -- print("debug mode")
    set_symbols('debug') -- 启用调试符号
    set_optimize('none') -- 禁用编译器优化
elseif is_mode('release') then
    -- print("release mode")
    set_symbols('hidden') -- 隐藏调试符号
    set_optimize('fastest') -- 启用最快优化 ==> -O3
end


-- 设置依赖
add_includedirs('include')
add_includedirs('third_lib')

-- 添加编译宏
add_defines("PROJECT_DIR=" .. "\"$(projectdir)\"")

-- 添加内部编译库
includes('third_lib')

-- 添加子模块
includes('grammar')
-- includes('dlib')
includes('network')
includes('opencv')
-- includes('eigen')
includes('qt')
includes('sdl')
includes('tools')

--[[
生成compile_commands.json:  `xmake project -k compile_commands`
设置编译模式: xmake f -m [debug|release]
设置运行前自动编译: xmake f --policies=run.autobuild
--]]
