/**
 * @file sdl_create_window.cpp
 * @author 逆流 (1171267147@qq.com)
 * @brief 使用SDL2创建一个空白窗口
 * @version 0.1
 * @date 2024-06-12
 *
 * @copyright Copyright (c) 2024.
 * @ref
 * https://real-zony.github.io/p/sdl-tutorial-series-chapter-1-getting-started-with-sdl/
 *
 * @par 修改日志:
 * <table>
 * <tr><th>Date       <th>Version <th>Author  <th>Description
 * <tr><td>2024-06-12 <td>V0.1    <td>逆流     <td>实现创建空白窗口
 * </table>
 */
#include <SDL2/SDL.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_timer.h>
#include <iostream>

int main(int argc, char* argv[]) {
    // 初始化SDL。这里 SDL_INIT_EVERYTHING 用于初始化所有子系统
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        std::cerr << "SDL_Init failed: " << SDL_GetError() << std::endl;
        return -1;
    }

    // 创建窗口。同时设置窗口标题、位置、大小和创建后立刻可见
    // 因为设置为立刻可见，因此在渲染前，窗口会出现一段时间黑屏
    SDL_Window* window =
        SDL_CreateWindow("SDL2 Window", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640, 480, SDL_WINDOW_SHOWN);

    if (window == nullptr) {
        std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
        return -1;
    }

    // 创建渲染器，并设置渲染器参数。
    // ?第二个参数是渲染器的驱动索引，-1表示使用默认的渲染驱动
    // SDL_RENDERER_ACCELERATED: 使用硬件加速渲染
    // SDL_RENDERER_PRESENTVSYNC: 渲染器与显示器刷新率同步
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    // 设置渲染器颜色
    SDL_SetRenderDrawColor(renderer, 0, 125, 250, 0);
    // 清空屏幕
    SDL_RenderClear(renderer);
    // 刷新屏幕或者将渲染缓冲区显示到屏幕上
    SDL_RenderPresent(renderer);
    // 停留窗口5s
    SDL_Delay(5000);

    SDL_Quit();
    return 0;
}
