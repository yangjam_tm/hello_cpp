add_requires('pkgconfig::sdl2', {alias = 'sdl2'})
add_requires('pkgconfig::SDL2_net', {alias = 'sdl2_net'}) -- 跨平台网络支持库
add_requires('pkgconfig::SDL2_gfx', {alias = 'sdl2_gfx'})
add_requires('pkgconfig::SDL2_ttf', {alias = 'sdl2_ttf'}) -- 用于加载TrueType字体
add_requires('pkgconfig::SDL2_image', {alias = 'sdl2_image'}) -- 用于加载图像
add_requires('pkgconfig::SDL2_mixer', {alias = 'sdl2_mixer'}) -- 用于音频支持

function build_sdl_target(filename)
    target(path.basename(filename))
        set_group('sdl')
        set_default(false)
        set_kind('binary')
        add_packages('sdl2', 'sdl2_image')
        add_files(filename)
    target_end()
end

for _, file in ipairs(os.files('*.cpp')) do
    -- print("Found cpp file: " .. file)
    build_sdl_target(file)
end
