/**
 * @file      draw_polygon.cpp
 * @author    yangjian(1171267147@qq.com)
 * @brief     opencv在图像中绘制多边形
 * @date      2023-11-06
 *
 * @copyright Copyright (c) 2023
 */
#include <opencv2/opencv.hpp>

using namespace cv;

int main(int argc, char** argv)
{
    Mat src_img(480, 640, CV_8UC3, Scalar(0, 0, 0));

    Point triange_points[3] = { Point(100, 100), Point(200, 200),
                                Point(300, 100) };
    // 点集应该相邻点排列
    Point quad_points[4] = { Point(400, 100), Point(500, 100), Point(600, 200),
                             Point(300, 200) };
    Point pent_points[5] = { Point(100, 400), Point(200, 400), Point(250, 450),
                             Point(200, 480), Point(50, 450) };

    const Point* set_addr[]   = { triange_points, quad_points, pent_points };
    int          point_nums[] = { 3, 4, 5 };

    /**
     * @brief  绘制多边形并填充为指定颜色
     * @param  src_img  待绘制的图像
     * @param  set_addr 多边形顶点集,保存每一组点集的首地址
     * @param  point_nums 多边形顶点集的每一组点的数量
     * @param  n 多边形的数量
     */
    fillPoly(src_img, set_addr, point_nums, 3, Scalar(0, 255, 255));
    // polylines(src_img, set_addr, point_nums, 3, true, Scalar(0, 255, 255),
    // 2);

    namedWindow("show");
    imshow("show", src_img);
    waitKey(0);
    destroyAllWindows();

    return 0;
}