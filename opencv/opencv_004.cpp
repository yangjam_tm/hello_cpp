/**
 * @file      env_test.cpp
 * @author    yangjian(1171267147@qq.com)
 * @brief     测试opencv环境--打开一个视频文件并在窗口中显示
 * @date      2023-11-04
 *
 * @copyright Copyright (c) 2023
 */
#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{
    cout << "OpenCV version: " << CV_VERSION << endl;

    VideoCapture cap("/home/yangjian/Workspace/Video/test.mp4");
    /***
     * [ WARN:0@0.009] global cap_gstreamer.cpp:1426 open OpenCV | GStreamer
     * warning: Error opening bin: syntax error [ WARN:0@0.009] global
     * cap_gstreamer.cpp:1164 isPipelinePlaying OpenCV | GStreamer warning:
     * GStreamer: pipeline have not been created
     * @error: 使用“~”描述用户路径，导致gstreamer报错
     */

    if (!cap.isOpened()) {
        cout << "Cannot open video file" << endl;
        return -1;
    }

    // 输出cap的视频信息
    cout << "Frame width: " << cap.get(CAP_PROP_FRAME_WIDTH) << endl;
    cout << "Frame height: " << cap.get(CAP_PROP_FRAME_HEIGHT) << endl;
    cout << "Frame count: " << cap.get(CAP_PROP_FRAME_COUNT) << endl;
    cout << "FPS: " << cap.get(CAP_PROP_FPS) << endl;

    auto sleepAsMsec = static_cast<int>(1e3 / cap.get(CAP_PROP_FPS));
    cout << "Sleep time: " << sleepAsMsec << " us" << endl;

    namedWindow("Video");

    do {
        Mat image;
        cap >> image;
        if (image.empty()) {
            cout << "End of video file" << endl;
            break;
        }

        imshow("Video", image);

        auto key = waitKey(sleepAsMsec);
        if (key == 27 /*ESC*/ || key == 'q' || key == 'Q') {
            break;
        }
    } while (true);

    destroyAllWindows();

    return 0;
}