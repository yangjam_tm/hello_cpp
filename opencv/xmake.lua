add_requires('pkgconfig::opencv4', {alias = 'opencv4'})


function build_case(filename)
    target(path.basename(filename))
    set_kind('binary')
    set_default(false)
    set_group('opencv')
    add_files(filename)
    -- set_targetdir('./')
    add_packages('opencv4')
    target_end()
end

-- build_case('env_test.cpp')
-- build_case('save_mat.cpp')
-- build_case('mat_memory.cpp')
-- build_case('scan_pixmap.cpp')
-- build_case('parallel_test.cpp')
-- build_case('switch_video.cpp')

for _, file in ipairs(os.files('*.cpp')) do
    -- print("Found cpp file: " .. file)
    build_case(file)
end
