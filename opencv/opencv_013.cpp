/**
 * @file cv_change_contrast_and_brightness.cpp
 * @author 逆流 (1171267147@qq.com)
 * @brief 图像的基础线性变换
 * @version 0.1
 * @date 2024-07-04
 *
 * @copyright Copyright (c) 2024.
 * @ref https://docs.opencv.org/4.x/d3/dc1/tutorial_basic_linear_transform.html
 */

#include <iostream>
#include <opencv2/opencv.hpp>
#include "env.h"

using std::cin;
using std::cout;
using std::endl;
using namespace cv;

int main(int argc, char *argv[])
{
    CommandLineParser parser(argc, argv, "{@input | lena.jpg | input image}");

    samples::addSamplesDataSearchPath(kResourcesDir);
    Mat src = imread(samples::findFile(parser.get<String>("@input")));

    if (src.empty()) {
        cout << "Could not open or find the image!\n" << endl;
        cout << "Usage: " << argv[0] << " <Input image>" << endl;
        return EXIT_FAILURE;
    }

    Mat dest = Mat::zeros(src.size(), src.type());

    double alpha = 1.0;
    int    beta  = 0;

    do {
        cout << " Basic Linear Transforms " << endl;
        cout << "-------------------------" << endl;
        cout << "* Enter the alpha value [1.0-3.0]: ";
        cin >> alpha;
        cout << "* Enter the beta value [0-100]: ";
        cin >> beta;

        for (int row = 0; row < src.rows; row++)
            for (int col = 0; col < src.cols; col++)
                for (int ch = 0; ch < src.channels(); ch++)
                    dest.at<Vec3b>(row, col)[ch] = saturate_cast<uchar>(alpha * src.at<Vec3b>(row, col)[ch] + beta);

        imshow("Original Image", src);
        imshow("New Image", dest);

        auto key = waitKey(0);
        if (key == 'q' || key == 27)
            break;

        destroyAllWindows();

    } while (true);
    return 0;
}