/**
 * @brief 使用opencv的FileStorage保存mat的原始数据
 */
#include <iostream>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

void write(Mat mat)
{
    FileStorage fs("mat.yml", FileStorage::WRITE);
    fs << "mat" << mat;
    fs.release();
}

void read(Mat& mat)
{
    FileStorage fs("mat.yml", FileStorage::READ);
    fs["mat"] >> mat;
    fs.release();
}

int main(void)
{
    Mat mat = Mat::eye(4, 4, CV_8UC1);
    write(mat);
    Mat temp;
    read(temp);
    cout << temp << endl;
}