/**
 * @brief 测试赋值过程中的mat内存拷贝问题
 */
// #include <deque>
#include <iostream>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

int main(void)
{
    Mat m1 = Mat::eye(4, 4, CV_8UC1);

    // 不会发生实际的拷贝，m1和m2指向同一块内存
    Mat m2               = m1;
    m2.at<uint8_t>(2, 2) = 2;

    cout << m1 << endl;
    printf("%p %p\n", m1.data, m2.data);

    Mat m3(4, 4, CV_8UC1);
    // 存在一块初始化内存，可以正常访问
    printf("m3 init: %p\n", m3.data);

    // 但是在执行赋值后，该内存指向了m1分配的那块内存
    m3                   = m1;
    m2.at<uint8_t>(2, 2) = 3;

    cout << m3 << endl;
    printf("%p %p\n", m1.data, m3.data);

    queue<Mat> mq;
    mq.push(Mat(2, 2, CV_8UC1, Scalar(0)));
    mq.push(Mat(2, 2, CV_8UC1, Scalar(1)));
    // TODO 复现实际工程中，多线程模式下，出现了m3被新的头节点覆盖的问题
    {
        m3 = mq.front();
        mq.pop();
    }
    mq.push(Mat(2, 2, CV_8UC1, Scalar(2)));

    cout << m3 << endl;
    printf("%p\n", m3.data);
    cout << mq.front() << endl;
    printf("%p\n", mq.front().data);
}