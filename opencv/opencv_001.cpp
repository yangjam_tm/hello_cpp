/**
 * @brief 测试VideoCapture动态切换视频源
 */
#include <iostream>
#include <opencv2/opencv.hpp>
#include <string>
#include <vector>

#include "env.h"

using namespace std;
using namespace cv;

int main(void)
{
    vector<string> play_list{RESOURCES_DIR + "test01.mp4",
                             RESOURCES_DIR + "test02.mp4"};

    std::size_t index = 0;
    std::size_t total = play_list.size();

    auto cap = VideoCapture(play_list[index]);

    if (cap.isOpened())
        cout << "open videocapture successful!" << endl;
    else
        cout << "failed!" << endl;

    const string title = "play video";

    namedWindow(title);

    // cap.set(cv::CAP_PROP_FPS, 30);

    while (true) {
        Mat image;

        cap >> image;

        imshow(title, image);

        auto key = waitKey(30);

        if (key == 'q') break;
        if (key == 's') {
            // 关闭视频文件或捕获文件，该接口在对象析构或再次调用open时自动调用
            // cap.release();
            index = (index + 1) % total;
            cap.open(play_list[index]);
            if (!cap.isOpened()) break;
        }
    }

    destroyWindow(title);
    cap.release();

    return 0;
}